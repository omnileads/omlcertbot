#!/bin/bash

# Configura Nginx para usar el FQDN proporcionado
FQDN=${FQDN:-example.com}
EMAIL=${EMAIL:-email@example.com}

# Reemplaza server_name en la configuración de Nginx
sed -i "s/server_name _;/server_name $FQDN;/" /etc/nginx/nginx.conf

# Asegurarse de que Nginx este corriendo
service nginx start

if [ "${RENEW_CERTIFICATE}" == "true" ]; then
    echo "Renovando los certificados existentes..."
    certbot renew --webroot -w /var/www/certbot --non-interactive
else
    echo "Generando un nuevo certificado para ${FQDN}..."
    certbot certonly --webroot -w /var/www/certbot -d $FQDN --email $EMAIL --agree-tos --non-interactive
fi
