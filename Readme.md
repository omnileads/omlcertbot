# Nginx certbot

This component facilitates the management of trusted certificates for later use in OMniLeads instances.

## Build

To build an image:

```
docker buildx build --file=Dockerfile --tag=$TAG --target=run .
```

Where $TAG is the docker tag you want for image. You can check the version.txt file for the tag.

## Deploy

First and foremost, we must ensure that Nginx is stopped on the instance.

```
systemctl stop nginx
```

Then we define two variables necessary to request the certificate:

```
export FQDN=your_domain.com
export EMAIL=your_email
```

We will place the certificates in these directories on the host:

```
mkdir -p /opt/certbot/letsencrypt
```

We proceed with the deployment of the container:

```
podman run -it --rm --name nginx-certbot \
  -e FQDN=${FQDN} \
  -e EMAIL=${EMAIL} \
  -e RENEW_CERTIFICATE=false \
  -v "/opt/certbot/letsencrypt:/etc/letsencrypt" \
  -v "/opt/certbot/webroot:/var/www/certbot" \
  -p 80:80 \
  docker.io/omnileads/certbot:240201.01
```

Now we need to overwrite the self-signed certificates that are provided in the default OMniLeads installation with the ones we generated recently.

```
cp /opt/certbot/letsencrypt/live/${FQDN}/cert.pem /etc/omnileads/certs/cert.pem
cp /opt/certbot/letsencrypt/live/${FQDN}/privkey.pem /etc/omnileads/certs/key.pem
```

Finally, we start Nginx:

```
systemctl start nginx
```

## Renew certificate

To renew the certificate, you must launch the container using the RENEW_CERTIFICATE=true variable.

```
export FQDN=your_domain.com
export EMAIL=your_email
systemctl stop nginx
```

```
podman run -it --rm --name nginx-certbot \
  -e FQDN=your_domain.com \
  -e EMAIL=your_email \
  -e RENEW_CERTIFICATE=true \
  -v "/opt/certbot/letsencrypt:/etc/letsencrypt" \
  -v "/opt/certbot/webroot:/var/www/certbot" \
  -p 80:80 \
  docker.io/omnileads/certbot:240201.01
  ```

Finally

```
cp /opt/certbot/letsencrypt/live/${FQDN}/cert.pem /etc/omnileads/certs/cert.pem
cp /opt/certbot/letsencrypt/live/${FQDN}/privkey.pem /etc/omnileads/certs/key.pem
systemctl start nginx
```
