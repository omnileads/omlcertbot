FROM ubuntu:latest

# Instalar nginx y certbot
RUN apt-get update && \
    apt-get install -y nginx certbot python3-certbot-nginx

# Limpiar archivos de APT
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Copiar los archivos de configuración y scripts al contenedor
COPY nginx.conf /etc/nginx/nginx.conf
COPY entrypoint.sh /entrypoint.sh

# Hacer el script ejecutable
RUN chmod +x /entrypoint.sh

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
